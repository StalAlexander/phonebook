

package stal.test.phonebook.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import org.hibernate.annotations.Cascade;



/** Класс - сущность Пользователь
 *
 * @author stal
 */
@Entity
@Table(name = "user")
public class User implements Serializable {
    
    private Integer User_id;    
    private String name;
    private Set<Phonenumber> phonenumbers = new HashSet<Phonenumber>();

    public User(){};
    
    public User(String name) {
        this.name = name;
    }
    
    @Id    
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "user_id", unique = true, nullable = false)
    public Integer getUser_id() {
        return User_id;
    }

    
    public void setUser_id(Integer User_id) {
        this.User_id = User_id;
    }
 

    @Column(name="name")
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }


   @OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
    public Set<Phonenumber> getPhonenumbers() {
        return phonenumbers;
   }
   
   public void setPhonenumbers(Set<Phonenumber> phonenumbers) {
        this.phonenumbers = phonenumbers;
    }

   
    public void addNumber(Phonenumber nmb){
      this.phonenumbers.add(nmb);
    }

    @Override
    public String toString() {
        return "User{" + "User_id=" + User_id + ", name=" + name + ", phonenumbers=" + phonenumbers + '}';
    }
    
}
