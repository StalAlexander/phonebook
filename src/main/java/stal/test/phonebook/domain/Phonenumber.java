

package stal.test.phonebook.domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/** Класс - сущность Номер
 *
 * @author stal
 */

@Entity
@Table(name = "phonenumber")
public class Phonenumber implements Serializable{
    
    private Integer id;
    private String number;

    public Phonenumber() {
    }

    public Phonenumber(String number) {
        this.number = number;
    }

    public Phonenumber(String number, User user) {
        this.number = number;
        this.user = user;
    }
    
    

    private User user;
    

    @Id    
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="id")

    public Integer getId() {
        return id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name="number")
    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id", nullable = false)
    public User getUser(){
        System.out.println("user" + user);
      return user;
    
    } 

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Phonenumber{" + "id=" + id + ", number=" + number + '}';
    }
    
    
    
}
