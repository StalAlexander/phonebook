
package stal.test.phonebook.service;

import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import stal.test.phonebook.domain.Phonenumber;
import stal.test.phonebook.domain.User;

/** Сервис для работы с записной книгой
 *
 * @author stal
 */
public class Phonebook {

    private SessionFactory sf;

    public Phonebook() {
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("context.xml");
        sf = (SessionFactory) ctx.getBean("sessionFactory");

    }

    /** Добавляет запись о пользователе и телефоне
     * 
     * @param userName
     * @param phone 
     */
    
    public void insert(String userName, String phone) {
        Session session = sf.openSession();
        Transaction tx = session.beginTransaction();
        Criteria criteria = session.createCriteria(User.class);
        User currentUser = (User) criteria.add(Restrictions.eq("name", userName)).uniqueResult();
        if (currentUser == null) {
            currentUser = new User(userName);
        }
        Phonenumber pn = new Phonenumber(phone, currentUser);
        currentUser.addNumber(pn);

        session.save(currentUser);
        tx.commit();
    }

    /** Удаляет запись из книги
     *  
     * @param userName пользователь
     * @param phone телефон
     */
    
    public void delete(String userName, String phone) {
        Session session = sf.openSession();
        Transaction tx = session.beginTransaction();
        Criteria criteria = session.createCriteria(User.class);
        User currentUser = (User) criteria.add(Restrictions.eq("name", userName)).uniqueResult();
        if (currentUser != null) {
            Phonenumber pn = new Phonenumber(phone, currentUser);
            session.delete(pn);
        }
        tx.commit();

    }
/** Переименовывает пользователя
 * 
 * @param oldName старое имя
 * @param newName новое имя
 */
    
    public void renameUser(String oldName, String newName) {
        Session session = sf.openSession();
        Transaction tx = session.beginTransaction();
        Criteria criteria = session.createCriteria(User.class);
        User currentUser = (User) criteria.add(Restrictions.eq("name", oldName)).uniqueResult();
        if (currentUser != null) {
            currentUser.setName(newName);
            session.save(currentUser);
        }
        tx.commit();
    }

    /** Отрисовка всей книги
     * @return 
     */
    @Override
    public String toString() {
        Session session = sf.openSession();
        Transaction tx = session.beginTransaction();
        Criteria criteria = session.createCriteria(User.class);
        List list = criteria.list();
        tx.commit();
        return list.toString();
    }

}
