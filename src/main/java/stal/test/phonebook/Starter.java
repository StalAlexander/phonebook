/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package stal.test.phonebook;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.transaction.annotation.Transactional;
import stal.test.phonebook.domain.Phonenumber;
import stal.test.phonebook.domain.User;
import stal.test.phonebook.service.Phonebook;

/**
 *
 * @author stal
 */
public class Starter {

    
    public static void main(String[] args) {

        Phonebook b = new Phonebook();
        b.insert("Sasha", "08-00-8767");
        b.delete("Sasha", "08-00-8767");
        b.renameUser("Sasha", "Vasya");
        System.out.println(b);
        
        /*
        
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("context.xml");
        SessionFactory sf = (SessionFactory)ctx.getBean("sessionFactory");
      
        
    //    User u = new User();
    //    u.setName("nam6");
        Session session = sf.openSession();
        Transaction  tx = session.beginTransaction();
    //    session.save(u); 
        
        
        Criteria criteria = session.createCriteria(User.class);
        User yourObject = (User)criteria.add(Restrictions.eq("name", "name")).uniqueResult();
        if (yourObject!=null){
            
            Phonenumber pn = new Phonenumber("ssf5");
            pn.setUser(yourObject);
            System.out.println(yourObject.getPhonenumbers().add(pn));
       //   yourObject.setName("sds");
       // System.out.println("u" + yourObject);
        //  session.save(yourObject); 
        }
               
       /*
        User u = new User("ashhhd");
         session.save(u);
        System.out.println("uuu" +u.getUser_id());
        u.addNumber(new Phonenumber("12jj3"));
         session.save(u);
         */       
      //  tx.commit();
        
    }
}
